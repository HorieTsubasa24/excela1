﻿namespace エクセル選択A1
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.エクセル選択A1 = new System.Windows.Forms.Label();
            this.fileListBox = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.checkBox_A1 = new System.Windows.Forms.CheckBox();
            this.checkBox_ActiveSheet = new System.Windows.Forms.CheckBox();
            this.checkBox_A1FirstSheet = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.checkBox_SaveAs = new System.Windows.Forms.CheckBox();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // エクセル選択A1
            // 
            this.エクセル選択A1.AutoSize = true;
            this.エクセル選択A1.Location = new System.Drawing.Point(41, 36);
            this.エクセル選択A1.Name = "エクセル選択A1";
            this.エクセル選択A1.Size = new System.Drawing.Size(128, 12);
            this.エクセル選択A1.TabIndex = 0;
            this.エクセル選択A1.Text = "選択ファイル(ドラッグ可能)";
            // 
            // fileListBox
            // 
            this.fileListBox.AllowDrop = true;
            this.fileListBox.FormattingEnabled = true;
            this.fileListBox.ItemHeight = 12;
            this.fileListBox.Location = new System.Drawing.Point(39, 54);
            this.fileListBox.Name = "fileListBox";
            this.fileListBox.Size = new System.Drawing.Size(562, 196);
            this.fileListBox.TabIndex = 1;
            this.fileListBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.fileListBox_DragDrop);
            this.fileListBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.fileListBox_DragEnter);
            this.fileListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fileListBox_KeyDown);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(383, 25);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "ファイルを選択";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Excel Worksheets|*.xlsx;*.xlsm;*.xlsb;*.xltx;*.xltm;*.xls;*.xlt;*.xls;*.xml;*.xml" +
    ";*.xlam;*.xla;*.xlw;*.xlr|All Files|*.*";
            this.openFileDialog.Multiselect = true;
            // 
            // checkBox_A1
            // 
            this.checkBox_A1.AutoSize = true;
            this.checkBox_A1.Checked = true;
            this.checkBox_A1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_A1.Location = new System.Drawing.Point(43, 275);
            this.checkBox_A1.Name = "checkBox_A1";
            this.checkBox_A1.Size = new System.Drawing.Size(196, 16);
            this.checkBox_A1.TabIndex = 3;
            this.checkBox_A1.Text = "すべてのシートのセル選択をA1にする";
            this.checkBox_A1.UseVisualStyleBackColor = true;
            this.checkBox_A1.CheckedChanged += new System.EventHandler(this.checkBox_A1_CheckedChanged);
            // 
            // checkBox_ActiveSheet
            // 
            this.checkBox_ActiveSheet.AutoSize = true;
            this.checkBox_ActiveSheet.Checked = true;
            this.checkBox_ActiveSheet.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_ActiveSheet.Location = new System.Drawing.Point(43, 297);
            this.checkBox_ActiveSheet.Name = "checkBox_ActiveSheet";
            this.checkBox_ActiveSheet.Size = new System.Drawing.Size(352, 16);
            this.checkBox_ActiveSheet.TabIndex = 4;
            this.checkBox_ActiveSheet.Text = "先頭のシートをアクティブにする(チェック無しでアクティブシートはそのまま)";
            this.checkBox_ActiveSheet.UseVisualStyleBackColor = true;
            // 
            // checkBox_A1FirstSheet
            // 
            this.checkBox_A1FirstSheet.AutoSize = true;
            this.checkBox_A1FirstSheet.Location = new System.Drawing.Point(272, 275);
            this.checkBox_A1FirstSheet.Name = "checkBox_A1FirstSheet";
            this.checkBox_A1FirstSheet.Size = new System.Drawing.Size(203, 16);
            this.checkBox_A1FirstSheet.TabIndex = 5;
            this.checkBox_A1FirstSheet.Text = "先頭のシートのセル選択のみA1にする";
            this.checkBox_A1FirstSheet.UseVisualStyleBackColor = true;
            this.checkBox_A1FirstSheet.CheckedChanged += new System.EventHandler(this.checkBox_A1FirstSheet_CheckedChanged);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(495, 315);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(106, 39);
            this.button2.TabIndex = 6;
            this.button2.Text = "処理を実行";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // checkBox_SaveAs
            // 
            this.checkBox_SaveAs.AutoSize = true;
            this.checkBox_SaveAs.Checked = true;
            this.checkBox_SaveAs.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_SaveAs.Location = new System.Drawing.Point(43, 319);
            this.checkBox_SaveAs.Name = "checkBox_SaveAs";
            this.checkBox_SaveAs.Size = new System.Drawing.Size(289, 16);
            this.checkBox_SaveAs.TabIndex = 7;
            this.checkBox_SaveAs.Text = "別名で保存する(ファイル名末尾に\'_○(数字)\'が入ります)";
            this.checkBox_SaveAs.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(495, 25);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(106, 23);
            this.button3.TabIndex = 8;
            this.button3.Text = "ファイル選択解除";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 366);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.checkBox_SaveAs);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.checkBox_A1FirstSheet);
            this.Controls.Add(this.checkBox_ActiveSheet);
            this.Controls.Add(this.checkBox_A1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.fileListBox);
            this.Controls.Add(this.エクセル選択A1);
            this.Name = "Form1";
            this.Text = "エクセル選択A1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label エクセル選択A1;
        private System.Windows.Forms.ListBox fileListBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.CheckBox checkBox_A1;
        private System.Windows.Forms.CheckBox checkBox_ActiveSheet;
        private System.Windows.Forms.CheckBox checkBox_A1FirstSheet;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox checkBox_SaveAs;
        private System.Windows.Forms.Button button3;
    }
}

