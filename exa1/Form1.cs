﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;

namespace エクセル選択A1
{
    public partial class Form1 : Form
    {
        private Size defaultSize;

        private Microsoft.Office.Interop.Excel.Application excel;
        private Workbooks books;

        public Form1()
        {
            InitializeComponent();
            this.defaultSize = this.fileListBox.Size;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.openFileDialog.ShowDialog();
            this.fileListBox.Items.AddRange(this.openFileDialog.FileNames);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                this.fileListBox.Items.RemoveAt(this.fileListBox.SelectedIndex);
            }
            catch
            {

            }
        }

        private void fileListBox_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<string> errorStrs = new List<string>();

            if (this.fileListBox.Items.Count == 0)
            {
                return;
            }

            if (MessageBox.Show("処理を実行しますか?", "", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }

            try
            {
                this.excel = new Microsoft.Office.Interop.Excel.Application();
                this.books = excel.Workbooks;

                List<string> paths = new List<string>();
                for (int i = 0; i < this.fileListBox.Items.Count; i++)
                {
                    string path = fileListBox.Items[i].ToString();
                    if (File.Exists(path))
                    {
                        paths.Add(path);
                    }
                    else
                    {
                        errorStrs.Add(path + "が存在しません。");
                    }
                }

                bool isSelectA1 = this.checkBox_A1.Checked;
                bool isSelectA1FirstSheet = this.checkBox_A1FirstSheet.Checked;
                bool isActiveFirstSheet = this.checkBox_ActiveSheet.Checked;
                bool isSaveAs = this.checkBox_SaveAs.Checked;

                foreach (string wbPath in paths)
                {
                    Workbook wb = excel.Workbooks.Open(wbPath);
                    Sheets sheets = wb.Sheets;
                    int sheetCnt = 0;

                    Worksheet nowActiveSheet = wb.ActiveSheet;
                    string firstSheetName = null;
                    foreach (Worksheet sheet in wb.Sheets)
                    {
                        ++sheetCnt;

                        if (sheetCnt == 1)
                        {
                            firstSheetName = sheet.Name;   
                            
                            // 先頭のシートのセル選択のみA1にする
                            if (isSelectA1FirstSheet)
                            {
                                sheet.Activate();
                                Microsoft.Office.Interop.Excel.Range r = sheet.Range["A1"];
                                r.Select();
                                break;
                            }
                        }

                        // すべてのシートのセル選択をA1にする
                        if (isSelectA1)
                        {
                            sheet.Activate();
                            Microsoft.Office.Interop.Excel.Range r = sheet.Range["A1"];
                            r.Select();
                        }
                    }

                    if (isActiveFirstSheet)
                    {
                        // 先頭のシートをアクティブにする
                        wb.Sheets[firstSheetName].Activate();
                    }
                    else
                    {
                        // アクティブなシートは変わらない
                        nowActiveSheet.Activate();
                    }


                    if (isSaveAs)
                    {
                        List<string> pathSplits = wbPath.Split('\\').ToList();
                        if (pathSplits.Count == 0)
                        {
                            errorStrs.Add(wbPath + "\nファイル名が不正で、別名保存に失敗しました。");
                            continue;
                        }

                        int cnt = 0;
                        string folderPath = null;
                        pathSplits.ForEach(s =>
                        {
                            if (cnt < pathSplits.Count - 1)
                                folderPath = folderPath + s + "\\";

                            ++cnt;
                        });

                        List<string> exts = pathSplits.LastOrDefault().Split('.').ToList();
                        string fileName = null;
                        string ext = "";
                        if (exts.Count > 1)
                        {
                            cnt = 0;
                            exts.ForEach(s =>
                            {
                                if (cnt < exts.Count - 1)
                                {
                                    fileName = fileName + s;
                                    if (exts.Count > 2 && cnt < exts.Count - 2)
                                        fileName = fileName + ".";
                                }

                                ++cnt;
                            });
                            ext = exts.Last();
                        }
                        else if (exts.Count == 1)
                        {
                            fileName = exts[0];
                        }
                        else
                        {
                            fileName = "A1File";
                        }

                        string path = null;
                        for (int extNum = 0; extNum < 999; extNum++)
                        {
                            string checkPath = folderPath + fileName + "_" + extNum.ToString() + "." + ext;
                            if (File.Exists(checkPath))
                            {
                                continue;
                            }
                            path = checkPath;
                            break;
                        }
                        if (string.IsNullOrEmpty(path))
                        {
                            errorStrs.Add(path + "\nファイル名の重複が999を超えました。\nファイル名を変更するか既存ファイルを削除してください。");
                            continue;
                        }

                        // 別名保存
                        wb.SaveAs(path);
                    }
                    else
                    {
                        // 上書き保存
                        wb.Save();
                    }
                }

                excel.Quit();
                Marshal.ReleaseComObject(this.excel);

                excel = null;
                MessageBox.Show("保存に成功しました。");
            }
            catch (Exception exception)
            {
                errorStrs.Add("不明なエラーが発生しました。\n" + exception.ToString());
            }
            finally
            {
                if (errorStrs.Count > 0)
                {
                    string errorMes = null;
                    errorStrs.ForEach(s => errorMes += s);
                    MessageBox.Show(errorMes);
                }

                if (this.excel != null)
                {
                    try
                    {
                        this.excel.Quit();
                        Marshal.ReleaseComObject(this.excel);
                    }
                    catch
                    {
                        Marshal.ReleaseComObject(this.excel);
                    }
                }
            }
        }

        private void checkBox_A1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBox_A1.Checked)
            {
                this.checkBox_A1FirstSheet.Checked = !this.checkBox_A1.Checked;
            }
        }

        private void checkBox_A1FirstSheet_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBox_A1FirstSheet.Checked)
            {
                this.checkBox_A1.Checked = !this.checkBox_A1FirstSheet.Checked;
            }
        }

        private void fileListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                try
                {
                    this.fileListBox.Items.RemoveAt(this.fileListBox.SelectedIndex);
                }
                catch
                {
                    // 握り潰し
                }
            }
        }

        private void fileListBox_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                foreach (string file in files)
                {
                    if (!File.Exists(file))
                    {
                        return;
                    }

                    string ext = null;
                    if (file.Length >= 5)
                    {
                        ext = file.Substring(file.Length - 5, 5);
                    }
                    else
                    {
                        ext = file;
                    }

                    StringComparison c = StringComparison.CurrentCultureIgnoreCase;
                    if (ext.Contains(".xlsx", c) || ext.Contains(".xlsm", c) || ext.Contains(".xlsb", c) ||
                        ext.Contains(".xltx", c) || ext.Contains(".xltm", c) || ext.Contains(".xls", c) ||
                        ext.Contains(".xlt", c) || ext.Contains(".xls", c) || ext.Contains(".xml", c) ||
                        ext.Contains(".xlam", c) || ext.Contains(".xla", c) || ext.Contains(".xlw", c))
                    {
                        this.fileListBox.Items.Add(file);
                    }
                }
            }
            catch
            {
                // 握り潰し
            }
        }

        private void fileListBox_DragEnter(object sender, DragEventArgs e)
        {
            try
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    e.Effect = DragDropEffects.Copy;
                }
            }
            catch
            {
                // 握り潰し
            }
        }
    }
}
